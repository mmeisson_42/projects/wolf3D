/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreedel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 13:39:16 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:06:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_btreedel(t_btree **abtree, void (*del)(void*, size_t))
{
	if (abtree && *abtree && (*abtree)->left)
		ft_btreedel(&((*abtree)->left), del);
	if (abtree && *abtree && (*abtree)->right)
		ft_btreedel(&((*abtree)->right), del);
	if (abtree && *abtree)
		ft_btreedelone(abtree, del);
}
