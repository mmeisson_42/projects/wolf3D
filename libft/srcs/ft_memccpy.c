/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:42 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/27 16:27:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char	*d;
	unsigned char	*s;
	unsigned char	cbis;

	d = (unsigned char*)dest;
	s = (unsigned char*)src;
	cbis = (unsigned char)c;
	while (n--)
	{
		*d = *(s++);
		if (*(d++) == cbis)
			return (d);
	}
	return (NULL);
}
