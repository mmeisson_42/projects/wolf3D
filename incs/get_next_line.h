/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 13:14:50 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/30 20:02:13 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "libft.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>

# ifndef EOF
#  if (defined _WIN32) || (defined __win64)
#   define EOF 24
#  else
#   define EOF -1
#  endif
# endif

# define BUFF_SIZE 32

typedef struct		s_file
{
	struct s_file	*next;
	char			buffer[BUFF_SIZE + 1];
	int				i;
	int				fd;
}					t_file;

int					get_next_line(const int fd, char **str);

#endif
