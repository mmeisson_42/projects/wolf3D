/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 05:58:43 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 17:19:49 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include <fcntl.h>
# include <SDL2/SDL.h>
# include <math.h>
# include <stdbool.h>
# include "get_next_line.h"
# include "libft.h"
# include "ft_stdio.h"

# define WIN_X 1200
# define WIN_Y 800

# define ANGLE_STEP (M_PI / 3.0) / (double)WIN_X

# define WALL_Y WIN_Y / 3

# define ERROR(str) exit(write(2, str, ft_strlen(str)))

typedef enum		e_mouse
{
	RIGHT_CLICK = 1,
	LEFT_CLICK = 2,
	MID_CLICK = 3,
	WH_UP = 4,
	WH_DOWN = 5,
	WH_RIGHT = 6,
	WH_LEFT = 7,
}					t_mouse;

typedef enum		e_keyboard
{
	PLUS = 24,
	MINUS = 27,
	KPLUS = 69,
	KMINUS = 78,
	SPACE = 49,
	LEFT = 123,
	RIGHT = 124,
	BOT = 125,
	TOP = 126,
	ESCAPE = 53,
	ZERO = 29,
	ONE = 18,
	TWO = 19,
	THREE = 20,
	FOUR = 21,
	FIVE = 23,
	SIX = 22,
	SEVEN = 26,
	EIGHT = 28,
	NINE = 25,
	S = 1,
}					t_keyboard;

typedef struct	s_coords
{
	double	x;
	double	y;
}				t_coords;

typedef struct	s_coord
{
	int	x;
	int	y;
}				t_coord;

typedef struct	s_map
{
	uint8_t		**map;
	size_t		x;
	size_t		y;
}				t_map;

typedef struct	s_datas
{
	SDL_Window		*win;
	SDL_Renderer	*renderer;
	SDL_Texture		*texture;
	uint32_t		*screen;
	t_map			*map;
	t_coords		pos;
	double			angle;
}				t_datas;

typedef struct	s_wall
{
	double		distance;
	uint32_t	color;
}				t_wall;

typedef struct	s_ray
{
	t_coords		hit;
	double			distance;
	uint32_t		color;
}				t_ray;

/*
**	parse.c
*/
t_map			*parse(const char *file);

/*
**	raycast.c
*/
t_ray			raycast(t_datas *wolf, double angle);
size_t			wall_size(double distance, double angle);

/*
**	draw.x
*/
void			draw(t_datas *wolf);

/*
**	basics.c
*/
double			distance(t_coords one, t_coords two);
void			img_put_pixel(uint32_t *screen, int x, int y,
		uint32_t color);

/*
**	hook.c
*/
int				key_hook(int keycode, t_datas *fractal);

/*
**	sdl_loop.c
*/
void			sdl_loop(t_datas *wolf);

#endif
