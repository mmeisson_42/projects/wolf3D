# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/14 21:39:23 by mmeisson          #+#    #+#              #
#    Updated: 2017/05/28 17:23:23 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC				= clang -fcolor-diagnostics

NAME			= wolf3D

CFLAGS			= -Wall -Werror -Wextra -MD

VPATH			= ./srcs/

SRCS			= parse.c basics.c draw.c raycast.c hook.c\
				  get_next_line.c wolf.c draw_line.c

INCS_PATH		= ./incs/ ./libft/incs/ ./printf/incs/

LIBS_PATH		= ./libft/ ./printf/

OBJS_PATH		= ./.objs/

OBJS			= $(SRCS:.c=.o)
OBJ				= $(addprefix $(OBJS_PATH),$(OBJS))
INCS			= $(addprefix -I,$(INCS_PATH))
LIBS			= $(addprefix -L,$(LIBS_PATH))
DEPS			= $(OBJ:.o=.d)

LIBS_NAME		= -lft -lm -lftprintf
LDFLAGS			= $(LIBS) $(LIBS_NAME) -framework SDL2


all: $(NAME)

$(NAME): $(OBJ)
	@$(foreach PATHS, $(LIBS_PATH), make -j42 -C $(PATHS);)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

$(OBJS_PATH)%.o: %.c
	mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

clean:
	@$(foreach PATHS, $(LIBS_PATH), make -j42 -C $(PATHS) clean;)
	rm -rf $(OBJS_PATH)

fclean:
	@$(foreach PATHS, $(LIBS_PATH), make -j42 -C $(PATHS) fclean;)
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re

-include $(DEPS)
