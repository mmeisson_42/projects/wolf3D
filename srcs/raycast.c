/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 18:11:19 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 17:16:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

size_t			wall_size(double distance, double angle)
{
	if (distance < 1.5)
		distance = 1.5;
	angle = 0;
	return (size_t)(1.5 * (double)WIN_Y / distance);
}

t_ray			cast_x(t_datas *wolf, double angle)
{
	t_ray		cast;
	t_coords	step;
	t_map		*m;

	m = wolf->map;
	step.y = tan(angle);
	step.x = (angle < M_PI / 2.0 || angle > 3.0 * M_PI / 2.0) ?
		1.0 : -1.0;
	cast.hit.x = (angle < M_PI / 2.0 || angle > 3.0 * M_PI / 2.0) ?
		ceil(wolf->pos.x) : floor(wolf->pos.x);
	cast.hit.y = (cast.hit.x - wolf->pos.x) * step.y + wolf->pos.y;
	if (angle >= M_PI / 2 && angle < 3.0 * M_PI / 2.0)
		step.y *= -1;
	while (cast.hit.x > 0 && cast.hit.y > 0 && cast.hit.x < m->x
			&& cast.hit.y < m->y &&
			((angle >= M_PI / 2.0 && angle < 3.0 * M_PI / 2.0 &&
			m->map[(long)cast.hit.y][(long)cast.hit.x - 1] == 0) ||
			((angle < M_PI / 2.0 || angle > 3.0 * M_PI / 2.0) &&
			m->map[(long)cast.hit.y][(long)cast.hit.x] == 0)))
	{
		cast.hit.x += step.x;
		cast.hit.y += step.y;
	}
	cast.distance = distance(wolf->pos, cast.hit);
	return (cast);
}

t_ray			cast_y(t_datas *wolf, double angle)
{
	t_ray		cast;
	t_coords	step;
	t_map		*m;

	m = wolf->map;
	cast.hit.y = (angle < M_PI) ? ceil(wolf->pos.y) : floor(wolf->pos.y);
	cast.hit.x = wolf->pos.x +
		(cast.hit.y - wolf->pos.y) / tan(angle);
	step.y = (angle < M_PI) ? 1.0 : -1.0;
	step.x = 1.0 / tan(angle);
	if (angle > M_PI && angle <= 2 * M_PI)
		step.x *= -1;
	while (cast.hit.x > 0 && cast.hit.y > 0 &&
			cast.hit.x < m->x && cast.hit.y < m->y &&
			((angle > M_PI &&
			m->map[(long)cast.hit.y - 1][(long)cast.hit.x] == 0) ||
			(angle < M_PI &&
			m->map[(long)cast.hit.y][(long)cast.hit.x] == 0)))
	{
		cast.hit.x += step.x;
		cast.hit.y += step.y;
	}
	cast.distance = distance(wolf->pos, cast.hit);
	return (cast);
}

t_ray			raycast(t_datas *wolf, double angle)
{
	t_ray	x;
	t_ray	y;

	x = cast_x(wolf, angle);
	y = cast_y(wolf, angle);
	if (x.distance < y.distance)
		return (x);
	else
		return (y);
}
