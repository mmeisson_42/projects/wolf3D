/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 18:09:04 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 17:04:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		floor_sky(t_datas *wolf)
{
	size_t		x;
	size_t		y;

	y = 0;
	while (y < WIN_Y / 2)
	{
		x = 0;
		while (x < WIN_X)
			img_put_pixel(wolf->screen, x++, y, 0x005555DD);
		y++;
	}
	while (y < WIN_Y)
	{
		x = 0;
		while (x < WIN_X)
			img_put_pixel(wolf->screen, x++, y, 0x0044aa44);
		y++;
	}
}

void		draw_column(t_datas *wolf, t_ray cast, size_t x, double angle)
{
	size_t		beg;
	size_t		end;
	size_t		size;
	size_t		color;

	if (round(cast.hit.x) == cast.hit.x)
	{
		if (angle >= M_PI / 2.0 && angle < 3.0 * M_PI / 2.0)
			color = 0x00ff0000;
		else
			color = 0x0000ff00;
	}
	else
	{
		if (angle > M_PI)
			color = 0x000000ff;
		else
			color = 0x00ff00ff;
	}
	size = wall_size(cast.distance, wolf->angle - angle);
	beg = WIN_Y / 2 - size / 2;
	end = beg + size;
	while (beg < end)
		img_put_pixel(wolf->screen, x, beg++, color);
}

void		draw(t_datas *wolf)
{
	double		angle;
	t_ray		cast;
	size_t		x;

	angle = wolf->angle - (M_PI / 6.0);
	if (angle < 0.0)
		angle += 2 * M_PI;
	x = 0;
	floor_sky(wolf);
	while (x++ < WIN_X)
	{
		cast = raycast(wolf, angle);
		angle += ANGLE_STEP;
		if (angle >= 2 * M_PI)
			angle -= 2 * M_PI;
		draw_column(wolf, cast, x, angle);
	}
	SDL_UpdateTexture(wolf->texture, NULL, wolf->screen,
			WIN_X * sizeof(Uint32));
	SDL_RenderClear(wolf->renderer);
	SDL_RenderCopy(wolf->renderer, wolf->texture, NULL, NULL);
	SDL_RenderPresent(wolf->renderer);
}
