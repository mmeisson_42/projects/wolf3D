/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 06:41:33 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 17:47:49 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void		set_angle(t_datas *wolf, double move)
{
	wolf->angle += move;
	if (wolf->angle < 0)
		wolf->angle += 2.0 * M_PI;
	else if (wolf->angle >= 2.0 * M_PI)
		wolf->angle -= 2.0 * M_PI;
}

static void		set_pos(t_datas *wolf, bool front, bool run)
{
	t_coords		new;
	double			distance;

	distance = run ? 0.12 : 0.05;
	if (front == true)
	{
		new.x = wolf->pos.x + cos(wolf->angle) * distance;
		new.y = wolf->pos.y + sin(wolf->angle) * distance;
	}
	else
	{
		new.x = wolf->pos.x - cos(wolf->angle) * distance;
		new.y = wolf->pos.y - sin(wolf->angle) * distance;
	}
	if (new.x >= 0 && new.y >= 0 && new.x < wolf->map->x &&
			new.y < wolf->map->y
			&& wolf->map->map[(int)floor(new.y)][(int)floor(new.x)] == 0)
	{
		wolf->pos.x = new.x;
		wolf->pos.y = new.y;
	}
}

void			sdl_loop(t_datas *wolf)
{
	const Uint8		*key_map = SDL_GetKeyboardState(NULL);
	SDL_Event		e;

	while (true)
	{
		SDL_PumpEvents();
		if (SDL_PollEvent(&e) && e.type == SDL_QUIT)
			break ;
		if (key_map[SDL_SCANCODE_ESCAPE] || key_map[SDL_QUIT])
			break ;
		else if (key_map[SDL_SCANCODE_RIGHT])
			set_angle(wolf, 0.015);
		else if (key_map[SDL_SCANCODE_LEFT])
			set_angle(wolf, -0.015);
		else if (key_map[SDL_SCANCODE_UP])
			set_pos(wolf, true, (bool)key_map[SDL_SCANCODE_R]);
		else if (key_map[SDL_SCANCODE_DOWN])
			set_pos(wolf, false, (bool)key_map[SDL_SCANCODE_R]);
		else
			continue ;
		draw(wolf);
	}
}
