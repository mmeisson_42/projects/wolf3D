/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   basics.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 05:54:10 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 17:03:09 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

double			distance(t_coords one, t_coords two)
{
	return (sqrt(
		pow(two.x - one.x, 2) +
			pow(two.y - one.y, 2)));
}

inline void		img_put_pixel(uint32_t *pixels, int x, int y, uint32_t color)
{
	if (x >= 0 && y >= 0 && x < WIN_X && y < WIN_Y)
	{
		pixels[x + (y * WIN_X)] = color;
	}
}
