/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 18:11:22 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 18:26:44 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void		wolf_init(t_datas *wolf)
{
	size_t	x;
	size_t	y;

	x = 0;
	wolf->pos.x = 0;
	wolf->pos.y = 0;
	while (x < wolf->map->x)
	{
		y = 0;
		while (y < wolf->map->y)
		{
			if (wolf->map->map[y][x] == 0)
			{
				wolf->pos.x = (double)x + 0.5;
				wolf->pos.y = (double)y + 0.5;
				break ;
			}
			y++;
		}
		x++;
	}
	wolf->angle = M_PI;
}

void			init_sdl(t_datas *wolf)
{
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
		exit(write(2, "SDL_Error\n", 10));
	SDL_CreateWindowAndRenderer(
			WIN_X, WIN_Y,
			SDL_WINDOW_ALLOW_HIGHDPI,
			&wolf->win,
			&wolf->renderer);
	wolf->texture = SDL_CreateTexture(
			wolf->renderer,
			SDL_PIXELFORMAT_ARGB8888,
			SDL_TEXTUREACCESS_STATIC,
			WIN_X, WIN_Y);
	if (!wolf->win || !wolf->renderer || !wolf->texture)
		exit(write(2, "SDL_Error\n", 10));
	if (!(wolf->screen = ft_memalloc(WIN_X * WIN_Y * sizeof(uint32_t))))
		exit(write(2, "malloc Error\n", 13));
	SDL_SetWindowTitle(wolf->win, "Wolf3D :: mmeisson@student.42.fr");
}

static void		map_del(t_map *m)
{
	size_t		i;

	i = 0;
	while (i < m->y)
	{
		free(m->map[i]);
		i++;
	}
	free(m->map);
	free(m);
}

int				main(int argc, char *argv[])
{
	t_datas wolf;

	if (argc == 2 && !ft_strequ(argv[1], "--help"))
	{
		wolf.map = parse(argv[1]);
		wolf_init(&wolf);
		init_sdl(&wolf);
		draw(&wolf);
		sdl_loop(&wolf);
		SDL_DestroyTexture(wolf.texture);
		SDL_DestroyRenderer(wolf.renderer);
		free(wolf.screen);
		map_del(wolf.map);
	}
	else
	{
		ft_putendl_fd("Usage : ./wolf3d (map_name)", 2);
	}
	return (0);
}
