/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 23:43:02 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/18 09:53:32 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

size_t		parse_number(char *str)
{
	size_t		number;
	size_t		i;

	i = 0;
	number = 0;
	while (str[i] != '\0')
	{
		while (str[i] == ' ')
			i++;
		while (str[i] >= '0' && str[i] <= '9')
			i++;
		if (str[i] != '\0' && str[i] != ' ')
			ERROR("Bad file's format\n");
		number++;
	}
	return (number);
}

size_t		parse_line(t_map *map, t_list **l_tab, char *str)
{
	size_t		i;
	size_t		tab_index;
	uint8_t		*tab;

	map->y++;
	i = parse_number(str);
	if (!(tab = ft_memalloc(sizeof(uint8_t) * (i + 1))))
		ERROR("Malloc Error\n");
	i = 0;
	tab_index = 0;
	while (str[i] != '\0')
	{
		while (str[i] == ' ')
			i++;
		tab[tab_index++] = (uint8_t)ft_atoi(str + i);
		while (str[i] && str[i] != ' ')
			i++;
	}
	ft_lstadd(l_tab, ft_lstnew_cpy(tab, sizeof(uint8_t)));
	if (map->x == 0)
		map->x = tab_index;
	else if (map->x != tab_index)
		ERROR("Bad file's format\n");
	return (i);
}

void		fill_map(t_map *wolf, t_list *tab)
{
	size_t		i;
	t_list		*prev;

	i = wolf->y;
	if (!(wolf->map = malloc(sizeof(uint8_t *) * (wolf->y + 1))))
		ERROR("Memory allocation failed ... :(\n");
	prev = NULL;
	(wolf->map)[i] = NULL;
	while (i-- > 0)
	{
		(wolf->map)[i] = (uint8_t *)tab->content;
		prev = tab;
		tab = tab->next;
		free(prev);
	}
}

t_map		*parse(const char *file)
{
	t_list		*tab;
	char		*str;
	t_map		*map;
	int			fd;
	int			ret;

	tab = NULL;
	if (!(map = ft_memalloc(sizeof(*map))))
		ERROR("Malloc Error\n");
	map->x = 0;
	map->y = 0;
	fd = open(file, O_RDONLY);
	if (fd == -1)
		ERROR("Error while opening file\n");
	while ((ret = get_next_line(fd, &str)) == 1)
	{
		parse_line(map, &tab, str);
		ft_strdel(&str);
	}
	if (ret == -1)
		ERROR("Error while reading file\n");
	if (!(map->map = ft_memalloc(sizeof(char *) * (map->y + 1))))
		ERROR("Malloc Error\n");
	fill_map(map, tab);
	return (map);
}
